# EURL Gasless transfer

Transfer gasless funds using Beacon wallet interaction and relay account

## Available Scripts

In the project directory, you can run:

### `npm i`

Install all deps from package.json

### `npm start`

Run & start UI on port 3000

## Relay account

The relay account is a wallet that will broadcast gasless operations from the user.
It will be up to the relay to pay XTZ gas + fees.
Set your own relay by editing the privat key of the ```relayPriKey``` variable
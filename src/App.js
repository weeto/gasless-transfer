import React from "react"
import { TezosToolkit } from '@taquito/taquito'
import { BeaconWallet } from '@taquito/beacon-wallet'
import { SigningType } from '@airgap/beacon-sdk'
import { RpcClient } from '@taquito/rpc'

const signer = require('@taquito/signer')
const node = 'https://granadanet.smartpy.io/'
const eurl = 'KT1CBGqq3ogtgd5ZYaHarwbpoaJuDKQgoYZK'
const relayPriKey = 'edskRkGrCdFnpHHP9Fj5QD5ZEMNSEBH6iWbACLp3HBz54x1CTxQZApgE8UR8wtUwANvnjLhv4ipkwmsH2AaKH3zMBgRyAsxCxe' // edit to set your own relay account
const explorer = 'https://granadanet.tzkt.io/'
const client = new RpcClient('https://granadanet.smartpy.io', 'NetXz969SFaFn8k')

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      processing: null,
      success: false,
      error: null,
      operationId: null,
      amount: 0,
      receiver: null,
    };
  };

  setAmount = (e) => {
    this.setState({amount: e.target.value})
  }

  setReceiver = (e) => {
    this.setState({receiver: e.target.value})
  }

  sendGasless = async() => {
    this.setState({processing: 'Connect your wallet with Beacon...', success: false, operationId: null, error: null})
    try{
      const Tezos = new TezosToolkit(node)
      const contract = await Tezos.contract.at(eurl)
      const storage = await contract.storage()
      const options = {
        name: 'GasLess Demo',
        iconUrl: 'https://ipfs.tzstats.com/ipfs/QmcqsYQn8pTxQr3P1dYpgYxQa6GQPmoBTSWQ8bpuFEuaqe',
        preferredNetwork: "granadanet",
      }
      const wallet = new BeaconWallet(options)
      await wallet.requestPermissions({
            network: {
              type: 'granadanet',
            },
      })
      const userAddress = await wallet.getPKH()
      this.setState({processing: `Connected with ${userAddress}. Sign payload to process gasless transfer.`})
      console.log(`User connected with ${userAddress}`)
      let counter = await storage.permits.get(userAddress)? (await storage.permits.get(userAddress)).counter : 0 //fetch counter to forge signature
      let micheline = {
        "data":{
           "prim":"Pair",
           "args":[
              {
                 "string": eurl
              },
              {
                 "prim":"Pair",
                 "args":[
                    {
                       "int": `${counter}`
                    },
                    {
                       "prim":"Pair",
                       "args":[
                          {
                             "string": userAddress
                          },
                          [
                             {
                                "prim":"Pair",
                                "args":[
                                   {
                                      "string": this.state.receiver
                                   },
                                   {
                                      "prim":"Pair",
                                      "args":[
                                         {
                                            "int":"0"
                                         },
                                         {
                                            "int": `${Math.round(parseFloat(this.state.amount) * 1000000)}`
                                         }
                                      ]
                                   }
                                ]
                             }
                          ]
                       ]
                    }
                 ]
              }
           ]
        },
        "type":{
           "prim":"pair",
           "args":[
              {
                 "prim":"address"
              },
              {
                 "prim":"pair",
                 "args":[
                    {
                       "prim":"nat"
                    },
                    {
                       "prim":"pair",
                       "args":[
                          {
                             "prim":"address"
                          },
                          {
                             "prim":"list",
                             "args":[
                                {
                                   "prim":"pair",
                                   "args":[
                                      {
                                         "prim":"address"
                                      },
                                      {
                                         "prim":"nat"
                                      },
                                      {
                                         "prim":"nat"
                                      }
                                   ]
                                }
                             ]
                          }
                       ]
                    }
                 ]
              }
           ]
        }
      }
      console.log(JSON.stringify(micheline));
      let packedData = await client.packData(micheline);
      console.log(`PACK : ${packedData.packed}`)
      let txs = [
        {
          to_: this.state.receiver,
          token_id: 0,
          amount: Math.round(parseFloat(this.state.amount) * 1000000),
        }
      ];
      Tezos.setWalletProvider(wallet)
      const response = await wallet.client.requestSignPayload({
            signingType: SigningType.MICHELINE,
            payload: packedData.packed,
      })
      console.log(`Signature: ${response.signature}`)
      const account = await wallet.client.getActiveAccount()
      console.log(`Public key: ${account.publicKey}`)
      let transfers = [{
        from_: account.publicKey, 
        txs: txs, 
        sig: response.signature
      }]
      Tezos.setProvider({ signer: await signer.InMemorySigner.fromSecretKey(relayPriKey) })
      const operation = await contract.methods.transfer_gasless(false, false, transfers).send(); // apply flat fees?, support fees e.g. 0.02% of amount, list of transfers
      this.setState({processing: `Awaiting operation confirmation. Please wait...`})
      await operation.confirmation()
      this.setState({operationId: operation.hash, success: true, processing: null, error: null})
    }catch(e){
      this.setState({operationId: null, success: false, processing: null, error: JSON.stringify(e)})
    }
  }

  render = () => (
    <div className="App">
      <h2>
        Gasless demo
      </h2>
      <p><small>Node: <strong>{node}</strong></small></p>
      <p><small>EURL contract: <strong>{eurl}</strong></small></p>
      <input type="text" placeholder="tz1 receiver" onChange={this.setReceiver}/>
      <input type="number" placeholder="Amount in EURL" onChange={this.setAmount}/>
      {!this.state.processing ?
        <button onClick={() => {this.sendGasless()}}>Send Gasless</button>
        :
        this.state.processing
      }
      {this.state.error && <p className="error">{this.state.error}</p>}
      {this.state.success && <p>Operation completed with ID <a href={`${explorer}${this.state.operationId}`}>{this.state.operationId}</a></p>}
    </div>
  );
}

export default App;
